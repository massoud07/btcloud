const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp();
// // Create and Deploy Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//

/**
 * Take in device location and calculate matrix based on that
 */
exports.cleanDbAndBuildMatrix = functions.https.onRequest((request, response) => {
    var db = admin.firestore();
    var location = request.query.location;
    var collectionName = `devices-${location}`;
    var collections = [
        'devices-uofr',
        'devices-downtown'
    ];
    console.log('location:', location);

    return db.collection(collectionName)
    .orderBy('lastAccess', 'desc').get()
    .then((querySnapshot) => {

        // // Delete duplicate devices and return list of unique mac addresses
        //
        return new Promise((resolve, reject) =>
        {
            try {
                var macs = {};
                querySnapshot.forEach((doc) => {
                    var data = doc.data();

                    if(!macs[data.mac]) {
                        macs[data.mac] = data;
                    }
                    else{
                        if(data.lastAccess._seconds < macs[data.mac].lastAccess._seconds){
                            console.log('deleting duplicate device', ' => ', data.mac);
                            db.collection(collectionName).doc(doc.id).delete();
                        }
                    }
                });

                resolve(macs);
            }
            catch(err) {
                reject(err);
            }
        });        
    })
    // // Find common MAC addresses that have have lastAccess attribute greater than in origin
    // // 
    .then((macs) => {
        var promises = [macs];
        var queries = {};

        for(col of collections){
            if(col === collectionName) {
                continue;
            }

            queries[col] = [];

            var query = db.collection(col);

            for(const key of Object.keys(macs)){
                promises.push(query.where('mac', '==', key).where('lastAccess', '>', macs[key].lastAccess).get());
            }
        }

        return Promise.all(promises);
    })
    // // Calculate average time difference for travel time to each destination
    // //
    .then((querySnapshots) => {
        var averageTimes = {};           
        var travelTimes = {};
        var macs = querySnapshots[0];
        var origin = collectionName.split('-')[1];

        // Find all time differences
        for(var i = 1; i < querySnapshots.length; i++){
            var querySnapshot = querySnapshots[i];
            var destCollection = querySnapshot._query._path.segments[0];
            var destination = destCollection.split('-')[1];

            if(!travelTimes[destination]){
                travelTimes[destination] = [];
            }
            
            for(doc of querySnapshot.docs) {
                var data = doc.data();
                var timeDiff = data.lastAccess._seconds - macs[data.mac].lastAccess._seconds;

                travelTimes[destination].push(timeDiff);                
            }
        }

        // Calculate average time in minutes
        for(const key of Object.keys(travelTimes)){
            var timesArr = travelTimes[key];
            var avgTime = 0;
            var totalTime = 0;
            
            if(timesArr.length > 0) {
                for(var time of timesArr){
                    totalTime += time;
                }
                
                avgTime = (totalTime / timesArr.length) /60;
            }

            averageTimes[key] = avgTime;
            console.log('average time to', key, ':', avgTime, 'minutes');
        }
        
        db.collection('odmatrix').doc(origin).set(averageTimes);
        
        return response.send(averageTimes);
    })
    .catch((error) => {
        return response.send(error);
    });
});

/**
 * Http function for getting db matrix for a given origin
 */
exports.getODMatrix = functions.https.onRequest((request, response) => {
    var db = admin.firestore();
    var origin = request.query.origin;
    response.set('Access-Control-Allow-Origin', "*");
    response.set('Access-Control-Allow-Methods', 'GET');

    db.collection('odmatrix').doc(origin).get().then((doc) =>{
        if(doc.exists){
            return response.send(JSON.stringify(doc.data()));
        }
        else{
            return response.send('data not found');
        }
    })
    .catch((err) => {
        return response(err);
    });
});